FROM java:8
EXPOSE 8080
ADD /target/BooksService-0.0.1-SNAPSHOT.jar BooksService-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "BooksService-0.0.1-SNAPSHOT.jar"]