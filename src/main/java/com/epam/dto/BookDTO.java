package com.epam.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class BookDTO {

	@ApiModelProperty(notes = "The database auto generated Book id", required = true)
	private Long id;

	@ApiModelProperty(notes = "The title must contain atleast 2 and maximum 100 characters", required = true)
	private String title;

	@ApiModelProperty(notes = "The author must contain atleast 2 and maximum 100 characters", required = true)
	private String author;

	@ApiModelProperty(notes = "The book status, whether it is available to issue.")
	private String status;

	public BookDTO(Long id, @Size(min = 2, max = 100) @NotBlank(message = "title must not be blank") String title,
			@Size(min = 2, max = 100) String author) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
	}

}