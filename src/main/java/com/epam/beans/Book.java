package com.epam.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ApiModel(description = "All details about the Book.")
@Getter
@Setter
@AllArgsConstructor
@ToString
@NoArgsConstructor
@Entity
public @Data class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	@Size(min = 2, max = 100)
	@NotBlank(message = "title must not be blank")
	private String title;

	@Column(nullable = false)
	@Size(min = 2, max = 100)
	private String author;

	private String status;

	public Book(Long id, @Size(min = 2, max = 100) @NotBlank(message = "title must not be blank") String title,
			@Size(min = 2, max = 100) String author) {
		super();
		this.id = id;
		this.title = title;
		this.author = author;
	}

}