package com.epam.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.beans.Book;
import com.epam.dto.BookDTO;
import com.epam.exceptions.BookNotFoundException;
import com.epam.repository.BooksRepository;

@Service
public class BookServicesImpl implements BookServices {
	private static final Logger logger = LogManager.getLogger(BookServicesImpl.class);
	private BooksRepository bookRepository;

	@Autowired
	public BookServicesImpl(BooksRepository bookRepository) {
		super();
		this.bookRepository = bookRepository;
	}

	@Override
	public List<BookDTO> getAllBooksList() {
		List<BookDTO> bookDTOList;
		try {
			List<Book> bookEntityList = bookRepository.findAll();
			bookDTOList = new ArrayList<>();
			bookEntityList
					.forEach(book -> bookDTOList.add(new BookDTO(book.getId(), book.getTitle(), book.getAuthor())));
			logger.info("displaying the books list.");
		} catch (NullPointerException e) {
			throw new BookNotFoundException("Unable to add Book");
		}
		return bookDTOList;
	}

	@Override
	public BookDTO findBook(Long bookId) {
		Book bookEntity = bookRepository.findById(bookId)
				.orElseThrow(() -> new BookNotFoundException("selected book not available"));
		logger.info("book with the id {} found.", bookId);
		return new BookDTO(bookEntity.getId(), bookEntity.getTitle(), bookEntity.getAuthor());
	}

	@Override
	public BookDTO addBook(BookDTO bookDTO) {
		BookDTO bookDTOAdded;
		try {
			Book bookEntity = convertDtoToEntity(bookDTO);
			bookEntity = bookRepository.save(bookEntity);
			System.out.println(bookEntity.getStatus());
			bookDTOAdded = new BookDTO(bookEntity.getId(), bookEntity.getTitle(), bookEntity.getAuthor());
			logger.info("book added successfully");
		} catch (NullPointerException e) {
			throw new BookNotFoundException("Unable to add Book");
		}
		return bookDTOAdded;
	}

	private Book convertDtoToEntity(BookDTO bookDTO) {
		return new Book(bookDTO.getId(), bookDTO.getTitle(), bookDTO.getAuthor());
	}

	@Override
	public void deleteBook(Long bookId) {
		bookRepository.deleteById(bookId);
		logger.info("book has been deleted succesfully");
	}

	@Override
	public BookDTO update(Long bookId, BookDTO bookDTO) {
		Book bookEntity = convertDtoToEntity(bookDTO);
		bookRepository.findById(bookId).map(legacybook -> bookRepository.save(bookEntity))
				.orElseThrow(() -> new BookNotFoundException("Book not found"));
		logger.info("book wit hid {} has been updated successfully", bookId);
		return new BookDTO(bookEntity.getId(), bookEntity.getTitle(), bookEntity.getAuthor());
	}

}
