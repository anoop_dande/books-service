package com.epam.services;

import java.util.List;

import com.epam.dto.BookDTO;

public interface BookServices {
	public List<BookDTO> getAllBooksList();

	public BookDTO findBook(Long bookId);

	public BookDTO addBook(BookDTO bookToBeAdded);

	public void deleteBook(Long bookId);

	public BookDTO update(Long bookId, BookDTO book);

}