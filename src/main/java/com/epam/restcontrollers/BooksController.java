package com.epam.restcontrollers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.dto.BookDTO;
import com.epam.services.BookServices;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RefreshScope
@RestController
@RequestMapping(path = "books")
@Api(value = "This is Book service. This api can be used to add, update, read and delete the book")
@ApiResponses(value = { @ApiResponse(code = 201, message = "Successfully added book"),
		@ApiResponse(code = 200, message = "The request has been succeeded "),
		@ApiResponse(code = 500, message = "Internal Server error"),
		@ApiResponse(code = 404, message = "The resource you are looking for is not found"),
		@ApiResponse(code = 400, message = "The requested resource is not allowed") })
public class BooksController {

	@Autowired
	BookServices bookServices;

	@ApiOperation(value = "Add a new book", response = BookDTO.class)
	@PostMapping
	public ResponseEntity<BookDTO> addBook(
			@ApiParam(value = "Book model that needs to be added", required = true) @Valid @RequestBody BookDTO bookDTO) {
		ResponseEntity<BookDTO> responseEntity;
		BookDTO bookDTOAdded = bookServices.addBook(bookDTO);
		responseEntity = new ResponseEntity<>(bookDTOAdded, HttpStatus.CREATED);
		return responseEntity;
	}

	@ApiOperation(value = "Find all Books", response = BookDTO.class, responseContainer = "List")
	@GetMapping
	public ResponseEntity<List<BookDTO>> getAllBooks() {
		return ResponseEntity.ok(bookServices.getAllBooksList());
	}

	@ApiOperation(value = "Update a existing book", response = BookDTO.class)
	@PutMapping("{bookId}")
	public ResponseEntity<BookDTO> update(
			@ApiParam(value = "BookId to update the book", required = true) @PathVariable Long bookId,
			@ApiParam(value = "Book model that needs to be added", required = true) @Valid @RequestBody BookDTO book) {
		return ResponseEntity.ok(bookServices.update(bookId, book));
	}

	@ApiOperation(value = "find a Book by bookId", response = BookDTO.class)
	@GetMapping("{bookId}")
	public ResponseEntity<BookDTO> getBookById(
			@ApiParam(value = "BookId to update the book", required = true) @PathVariable Long bookId) {
		return ResponseEntity.ok(bookServices.findBook(bookId));
	}

	@ApiOperation(value = "Delete a Book by bookId", response = BookDTO.class)
	@DeleteMapping("{bookId}")
	public ResponseEntity<?> deleteBook(
			@ApiParam(value = "BookId to update the book", required = true) @PathVariable Long bookId) {
		bookServices.deleteBook(bookId);
		return ResponseEntity.noContent().build();
	}

}
