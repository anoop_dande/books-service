package com.epam.restcontrollers;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.epam.exceptions.BookNotFoundException;

@ControllerAdvice
public class BookServicesError {

	@ExceptionHandler({ BookNotFoundException.class })
	public ResponseEntity<Object> handleBookNotFoundException(BookNotFoundException exception) {
		ErrorDTO error = new ErrorDTO(HttpStatus.NOT_FOUND, exception.getMessage());
		return new ResponseEntity<>(error, new HttpHeaders(), error.getStatusOfError());
	}

}
