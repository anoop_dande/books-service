//package com.epam.repository;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//
//import java.util.List;
//
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//
//import com.epam.beans.Book;
//
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = Replace.NONE)
//class BooksRepositoryTest {
//
//	@Autowired
//	BooksRepository booksRepository;
//
//	@Test
//	public void testSave() {
//		Book book = new Book();
//		book.setId(1L);
//		book.setTitle("Gita Rahasya");
//		book.setAuthor("Bal Gangadhar Tilak");
//		booksRepository.save(book);
//		assertEquals("Bal Gangadhar Tilak", book.getAuthor());
//	}
//
//	@Test
//	void testFindAll() {
//		Book book = new Book();
//		book.setId(1L);
//		book.setTitle("Gita Rahasya");
//		book.setAuthor("Bal Gangadhar Tilak");
//		booksRepository.save(book);
//		List<Book> bookList = booksRepository.findAll();
//		assertThat(bookList).isNotEmpty();
//	}
//
//	@Test
//	void testFindById() {
//		Book book = new Book();
//		book.setId(1L);
//		book.setTitle("Gita Rahasya");
//		book.setAuthor("Bal Gangadhar Tilak");
//		booksRepository.save(book);
//		assertEquals("Bal Gangadhar Tilak", booksRepository.findById(1L).get().getAuthor());
//	}
//
//	@Test
//	void testDelete() {
//		Book book = new Book();
//		book.setId(1L);
//		book.setTitle("Gita Rahasya");
//		book.setAuthor("Bal Gangadhar Tilak");
//		booksRepository.save(book);
//		booksRepository.deleteById(1L);
//		assertThat(booksRepository.findById(1L)).isEmpty();
//	}
//
//}
