package com.epam.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.epam.beans.Book;
import com.epam.dto.BookDTO;
import com.epam.repository.BooksRepository;

class BookServicesImplTest {

	@Mock
	BookServices bookServices;

	@InjectMocks
	BookServicesImpl bookServicesImpl;

	@Mock
	BooksRepository booksRepository;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testGetAllBooksList() {
		List<Book> bookList = new ArrayList<Book>();
		Book book = new Book(1L, "book1", "author1");
		bookList.add(book);
		when(booksRepository.findAll()).thenReturn(bookList);
		assertNotNull(bookServicesImpl.getAllBooksList());
	}

	@Test
	void testFindBook() {
		BookDTO bookDTO = new BookDTO(1L, "book1", "author1");
		Book book = new Book(1L, "book1", "author1");
		Optional<Book> optionalBook = Optional.ofNullable(book);
		when(booksRepository.findById(1L)).thenReturn(optionalBook);
		assertEquals(bookDTO, bookServicesImpl.findBook(1L));
	}

	@Test
	void testAddBook() {
		Book book = new Book(1L, "book1", "author1");
		BookDTO bookDTO = new BookDTO(1L, "book1", "author1");
		when(booksRepository.save(Mockito.any())).thenReturn(book);
		assertEquals(bookDTO, bookServicesImpl.addBook(bookDTO));
	}

	@Test
	void testDeleteBook() {
		doNothing().when(bookServices).deleteBook(1L);
		bookServicesImpl.deleteBook(1L);
		verify(booksRepository, times(1)).deleteById(1L);
	}

	@Test
	void testUpdate() {
		BookDTO bookDTO = new BookDTO(1L, "book1", "author1");
		Book book = new Book(1L, "book1", "author1");
		Book previousBook = new Book(1L, "book2", "author1");
		Optional<Book> optionalBook = Optional.ofNullable(previousBook);
		when(booksRepository.findById(1L)).thenReturn(optionalBook);
		when(booksRepository.save(book)).thenReturn(book);
		assertEquals(bookDTO, bookServicesImpl.update(1L, bookDTO));
	}

}
