package com.epam.restcontrollers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.epam.beans.Book;
import com.epam.dto.BookDTO;
import com.epam.services.BookServices;

class BooksControllerTest {

	private MockMvc mockMVC;

	@Mock
	BookServices bookServices;

	@InjectMocks
	BooksController booksController;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
		this.mockMVC = MockMvcBuilders.standaloneSetup(booksController).setControllerAdvice(new BookServicesError())
				.build();
	}

	static List<BookDTO> bookList;

	@BeforeAll
	public static void setInitVars() {
		bookList = new ArrayList<BookDTO>();
		bookList.add(new BookDTO());
	}

	@Test
	public void testAddBook() {
		BookDTO book = new BookDTO(1L, "book1", "author1");
		when(bookServices.addBook(book)).thenReturn(book);
		ResponseEntity<BookDTO> responseEntity = booksController.addBook(book);
		assertThat(responseEntity.getBody().equals(book));

	}

	@Test
	void testGetAllBooks() {
		new Book(1L, "book1", "author1");
		when(bookServices.getAllBooksList()).thenReturn(bookList);
		ResponseEntity<List<BookDTO>> responseEntity = booksController.getAllBooks();
		assertThat(responseEntity.getBody().equals(bookList));
	}

	@Test
	void testUpdate() {
		BookDTO book = new BookDTO(1L, "book2", "author1");
		when(bookServices.update(1L, book)).thenReturn(book);
		ResponseEntity<BookDTO> responseEntity = booksController.update(1L, book);
		assertThat(responseEntity.getBody().equals(book)).isTrue();
	}

	@Test
	void testGetBook() {
		BookDTO book = new BookDTO(1L, "book1", "author1");
		when(bookServices.findBook(1L)).thenReturn(book);
		ResponseEntity<BookDTO> responseEntity = booksController.getBookById(1L);
		assertThat(responseEntity.getBody().equals(book));
	}

	@Test
	void testDelete() throws Exception {
		doNothing().when(bookServices).deleteBook(1L);
		booksController.deleteBook(1L);
		assertEquals(ResponseEntity.noContent().build(), booksController.deleteBook(1L));
		this.mockMVC.perform(delete("/books/1")).andExpect(status().isNoContent()).andExpect(content().string(""));
	}

}
