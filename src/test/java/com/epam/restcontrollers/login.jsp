<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>login</title>
<style>
.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
body, html{
  height: 100%;
  background: #a8d5fc;
	font-family: 'Lato', sans-serif;
}

.cartButton
{
background-color: #038cfc;
 color: white;
 padding: 2px 5px;
 border: none;
  border-radius: 4px;
  cursor: pointer;
}
h1{
color: #112330;
  text-align: center;
}
</style>
</head>

<body onload='document.loginForm.username.focus();'>
<h1>Welcome to the Epam Ecommerce</h1>
  <div class="login-page">	
  <div class="form">
   <c:if test="${not empty errorMessge}"><div style="color:red; font-weight: bold; margin: 30px 0px;">${errorMessge}</div></c:if>
    <form name='login' class="login-form" action="/login" method='POST'>
      <input type="text" name='username' placeholder="username"/>
      <input type='password' name='password' placeholder="password"/>
      <input name="submit" class="cartButton" type="submit" value="login" />
      <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> 
    </form>
  </div>
</div>
</body>
</html>